setup:
	composer install --ignore-platform-reqs

up:
	./vendor/bin/sail up -d
	./vendor/bin/sail artisan key:generate
	./vendor/bin/sail ps

down:
	./vendor/bin/sail down

migrate:
	./vendor/bin/sail artisan migrate

seed:
	./vendor/bin/sail artisan db:seed
