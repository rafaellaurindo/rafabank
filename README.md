## About Rafa Bank

RafaBank is an open-source platform with banking services, build using Laravel.

### Technologies

- [Compose](https://getcomposer.org/).
- [Laravel](https://laravel.com).
- [Laravel Sail](https://laravel.com/docs/8.x/sail).
- [Docker](https://www.docker.com).
- [Redis](https://redis.io/).
- [Docker Compose](https://docs.docker.com/compose/).

### Documentation

Access the official RafaBank's API documentation [here](https://documenter.getpostman.com/view/13892160/Tzm6mwHE)

### Using

To run this project, using Docker by Laravel Sail, follow these steps:

1. Clone this repository to your workspace.
2. Copy the `.env.example` file to `.env`: `cp .env.example .env`.
3. Run the `make setup` command to install the composer dependencies, or if your prefer run `composer install --ignore-platform-reqs`.
4. Run the `make up` command to start the docker containers (APP, Mysql and Redis).
5. When the containers have been up, await for around 15 seconds to grant they are read and run `make migrate` to create migrate the database tables.
6. Run the `make seed` to create the initial data.

## Contributing

Thank you for considering contributing to the RafaBank!

## License

The RafaBank is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
