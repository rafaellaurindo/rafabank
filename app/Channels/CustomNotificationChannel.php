<?php

namespace App\Channels;

use App\Clients\CustomNotificationApiClient;
use App\Models\User;
use Illuminate\Notifications\Notification;

class CustomNotificationChannel
{
    private CustomNotificationApiClient $notificationClient;

    public function __construct()
    {
        $this->notificationClient = app(CustomNotificationApiClient::class);
    }

    /**
     * Send the given notification though my custom channel.
     *
     * @param  \App\Models\User  $user
     * @param  Notification  $notification
     * @return void
     * @throws \App\Exceptions\SendNotificationException
     */
    public function send(User $user, Notification $notification)
    {
        $message = $notification->toCustomChannel($user);
        $this->notificationClient->sendNotification($user, $message);
    }
}
