<?php

namespace App\Clients;

use App\Exceptions\SendNotificationException;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class CustomNotificationApiClient
{
    const MESSAGE_SENT_RESPONSE = 'Success';

    private string $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Send notification to an User.
     *
     * @param  \App\Models\User  $user
     * @param  string  $message
     * @return bool
     * @throws \App\Exceptions\SendNotificationException
     */
    public function sendNotification(User $user, string $message): bool
    {
        $response = Http::baseUrl($this->baseUrl)
            ->get('/notify', [
                'user_id' => $user->id,
                'message' => $message,
            ]);

        if ($response->failed() || $response->json('message') !== self::MESSAGE_SENT_RESPONSE) {
            throw new SendNotificationException();
        }

        return true;
    }

    /**
     * Get Api Base Url
     *
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }
}
