<?php

namespace App\Clients;

use App\Exceptions\GetTransactionAuthorizationException;
use App\Models\Transfer;
use Illuminate\Support\Facades\Http;

class TransactionAuthorizerApiClient
{
    const AUTHORIZED_TRANSFER_MESSAGE = 'Autorizado';

    private string $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Check if transfer has been authorized.
     *
     * @param  \App\Models\Transfer  $transfer
     * @return bool
     * @throws \App\Exceptions\GetTransactionAuthorizationException
     */
    public function checkAuthorizationToPerformTransfer(Transfer $transfer): bool
    {
        $response = Http::baseUrl($this->getBaseUrl())
            ->get('/8fafdd68-a090-496f-8c9a-3442cf30dae6', [
                'payer_id' => $transfer->payer->id,
                'payee_id' => $transfer->payee->id,
                'amount' => $transfer->amount,
                'device' => request()->getClientIp(),
            ]);

        if ($response->failed()) {
            throw new GetTransactionAuthorizationException();
        }

        return $response->json('message') === self::AUTHORIZED_TRANSFER_MESSAGE;
    }

    /**
     * Get Api Base Url
     *
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }
}
