<?php

namespace App\Exceptions;

class CompanyUserCannotSendTransfersException extends GenericApiException
{
    protected int $statusCode = 403;
    protected string $error = 'transfer.company_cant_sent_transfers';
    protected $message = 'Company users cannot send transfers.';
}
