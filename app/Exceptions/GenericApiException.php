<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class GenericApiException extends HttpException
{
    protected int $statusCode = 500;
    protected string $error = 'api.generic_error';
    protected $message = 'An error has been happened.';

    public function __construct()
    {
        $this->code = $this->getStatusCode();
        parent::__construct($this->getStatusCode(), $this->message);
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
