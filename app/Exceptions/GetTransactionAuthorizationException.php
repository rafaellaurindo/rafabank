<?php

namespace App\Exceptions;

class GetTransactionAuthorizationException extends GenericApiException
{
    protected int $statusCode = 502;
    protected string $error = 'transaction.failed_get_authorization';
    protected $message = 'Was unable to obtain, authorization to perform the transaction. Please try again later or contact us.';
}
