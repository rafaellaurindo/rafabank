<?php

namespace App\Exceptions;

use Doctrine\DBAL\Query\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (ValidationException $e) {
            return response()->json([
                'error' => 'validation.failed',
                'message' => $e->getMessage(),
                'data' => $e->errors(),
            ], 422);
        });

        $this->renderable(function (QueryException $e) {
            return response()->json([
                'error' => 'database.query_exception',
                'message' => $e->getMessage(),
                'data' => [],
            ], 500);
        });

        $this->renderable(function (NotFoundHttpException $e) {
            return response()->json([
                'error' => 'http.not_found',
                'message' => 'The requested resource was not found.',
                'data' => [],
            ], $e->getStatusCode());
        });

        $this->renderable(function (GenericApiException $e) {
            return response()->json([
                'error' => $e->getError(),
                'message' => $e->getMessage(),
                'data' => [],
            ], $e->getStatusCode());
        });

        $this->renderable(function (Throwable $e) {
            $status = 500;

            if ($e instanceof HttpException) {
                $status = $e->getStatusCode();
            }

            return response()->json([
                'error' => 'api.error',
                'message' => $e->getMessage(),
                'data' => [],
            ], $status);
        });
    }
}
