<?php

namespace App\Exceptions;

class InsufficientUserBalanceException extends GenericApiException
{
    protected int $statusCode = 400;
    protected string $error = 'transaction.insufficient_user_balance';
    protected $message = 'User does not have enough balance.';
}
