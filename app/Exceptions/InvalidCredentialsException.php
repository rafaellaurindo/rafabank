<?php

namespace App\Exceptions;

class InvalidCredentialsException extends GenericApiException
{
    protected int $statusCode = 401;
    protected string $error = 'auth.invalid_credentials';
    protected $message = 'Invalid credentials.';
}
