<?php

namespace App\Exceptions;

class InvalidUserTypeException extends GenericApiException
{
    protected int $statusCode = 400;
    protected string $error = 'user.invalid_type';
    protected $message = 'The user type is invalid.';
}
