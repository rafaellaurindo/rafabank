<?php

namespace App\Exceptions;

class SendNotificationException extends GenericApiException
{
    protected int $statusCode = 502;
    protected string $error = 'notification.failed_sent';
    protected $message = 'Was unable to send notification.';
}


