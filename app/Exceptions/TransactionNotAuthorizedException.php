<?php

namespace App\Exceptions;

class TransactionNotAuthorizedException extends GenericApiException
{
    protected int $statusCode = 403;
    protected string $error = 'transaction.not_authorized';
    protected $message = 'The transaction was not authorized. Sorry for the inconvenience, please contact us.';
}

