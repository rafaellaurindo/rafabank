<?php

namespace App\Exceptions;

class UnauthenticatedException extends GenericApiException
{
    protected int $statusCode = 401;
    protected string $error = 'auth.unauthenticated';
    protected $message = 'Unauthenticated.';
}
