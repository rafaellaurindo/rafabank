<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private AuthService $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Create user auth token.
     *
     * @param  \App\Http\Requests\LoginRequest  $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\InvalidCredentialsException
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $user = $this->authService->login(
            email: $request->input('email'),
            password: $request->input('password'),
        );

        $authToken = $this->authService->createToken($user);

        return response()->json([
            'user' => $user,
            'token' => $authToken,
        ]);
    }

    /**
     * Get logged user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Revoke current user token.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function revokeToken(Request $request): JsonResponse
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([], 204);
    }
}
