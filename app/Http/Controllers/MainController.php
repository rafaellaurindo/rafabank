<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class MainController extends Controller
{
    public function healthy(): JsonResponse
    {
        return response()->json([
            'api_version' => config('app.version'),
            'status' => 'ok',
        ]);
    }

    public function redirectToDocumentation(): Redirector|RedirectResponse|Application
    {
        return redirect('https://documenter.getpostman.com/view/13892160/Tzm6mwHE');
    }
}
