<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTransferRequest;
use App\Jobs\SendNotificationJob;
use App\Models\Transfer;
use App\Models\User;
use App\Notifications\ReceivedTransferNotification;
use App\Services\TransferService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class TransferController extends Controller
{
    public TransferService $transferService;
    public UserService $userService;

    public function __construct(TransferService $transferService, UserService $userService)
    {
        $this->transferService = $transferService;
        $this->userService = $userService;
    }

    /**
     * List all transfers from user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $perPage = (int) $request->query('limit', 50);
        $transfers = auth()->user()->transfers()->simplePaginate($perPage);

        return response()->json($transfers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTransferRequest  $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CompanyUserCannotSendTransfersException
     * @throws \App\Exceptions\InsufficientUserBalanceException
     * @throws \App\Exceptions\TransactionNotAuthorizedException
     * @throws \Throwable
     */
    public function store(StoreTransferRequest $request): JsonResponse
    {
        /** @var User $payer */
        $payer = auth()->user();
        $amount = $request->input('amount');

        $payee = User::findOrFail($request->input('payee_id'));

        try {
            DB::beginTransaction();

            $this->userService
                ->setUser($payer)
                ->decreaseBalance($amount);

            $this->userService
                ->setUser($payee)
                ->increaseBalance($amount);

            $transfer = $this->transferService
                ->checkUserCanSendTransfers($payer)
                ->create(
                    payer: $payer,
                    payee: $payee,
                    amount: $amount
                )
                ->checkForAuthorization()
                ->getTransfer();

            dispatch(new SendNotificationJob($payee, new ReceivedTransferNotification($transfer)));

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();

            throw $e;
        }

        return response()->json($transfer->load([
            'payer:id,name,email',
            'payee:id,name,email',
        ]));
    }

    /**
     * Show a transfer.
     *
     * @param  \App\Models\Transfer  $transfer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Transfer $transfer): JsonResponse
    {
        return response()->json($transfer);
    }
}
