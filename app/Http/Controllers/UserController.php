<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUserRequest  $request
     * @return JsonResponse
     * @throws \App\Exceptions\InvalidUserTypeException
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        $user = $this->userService->create(
            name: $request->input('name'),
            email: $request->input('email'),
            document: $request->input('document'),
            password: $request->input('password'),
            type: $request->input('type'),
        );

        return response()->json($user);
    }
}
