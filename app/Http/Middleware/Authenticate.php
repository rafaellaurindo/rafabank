<?php

namespace App\Http\Middleware;

use App\Exceptions\UnauthenticatedException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * @throws \App\Exceptions\UnauthenticatedException
     */
    protected function unauthenticated($request, array $guards)
    {
        throw new UnauthenticatedException();
    }
}
