<?php

namespace App\Http\Requests;

use App\Rules\HasEnoughBalance;
use Illuminate\Foundation\Http\FormRequest;

class StoreTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        /** @var \App\Models\User $currentUser */
        $currentUser = auth()->user();

        return [
            'payee_id' => "required|numeric|exists:users,id|not_in:{$currentUser->id}",
            'amount' => [
                'bail',
                'required',
                'numeric',
                'min:0.1',
                new HasEnoughBalance($currentUser),
            ],
        ];
    }
}
