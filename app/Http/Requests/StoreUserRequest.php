<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $userDocumentRule = match ($this->input('type')) {
            User::TYPE_PERSON => 'cpf',
            User::TYPE_COMPANY => 'cnpj',
            default => 'cpf_cnpj',
        };

        return [
            'name' => 'required|string|max:255',
            'document' => "required|string|unique:users|{$userDocumentRule}",
            'email' => 'required|email|unique:users',
            'password' => ['required', 'confirmed', Password::min(8)->mixedCase()->numbers()],
            'type' => ['required', 'string', Rule::in(User::TYPES)],
        ];
    }

    public function messages(): array
    {
        return [
            'type.in' => trans('validation.custom.user_type.in'),
            'document.cpf' => trans('validation.custom.document.cpf'),
            'document.cnpj' => trans('validation.custom.document.cnpj'),
            'document.cpf_cnpj' => trans('validation.custom.document.cpf_cnpj'),
        ];
    }
}
