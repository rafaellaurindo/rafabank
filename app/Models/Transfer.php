<?php

namespace App\Models;

use App\Scopes\TransferFromAccountScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin IdeHelperTransfer
 */
class Transfer extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_payer_id', 'user_payee_id', 'amount',
    ];

    protected $hidden = [
        'user_payer_id', 'user_payee_id',
    ];

    protected $with = [
        'payer:id,name,email',
        'payee:id,name,email',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new TransferFromAccountScope());
    }

    public function payer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_payer_id');
    }

    public function payee(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_payee_id');
    }
}
