<?php

namespace App\Notifications;

use App\Channels\CustomNotificationChannel;
use App\Models\Transfer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReceivedTransferNotification extends Notification
{
    use Queueable;

    private Transfer $transfer;
    private string $amount;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Transfer $transfer)
    {
        $this->transfer = $transfer;
        $this->amount = currency($transfer->amount);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via(): array
    {
        return ['mail', CustomNotificationChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(): MailMessage
    {
        return (new MailMessage)
            ->subject('You received a transfer.')
            ->line("You received **{$this->amount}** from **{$this->transfer->payer->name}**.")
            ->line('This value is already yielding in your account!');
    }

    /**
     * Get the message to send thought custom notification service.
     *
     * @return string
     */
    public function toCustomChannel(): string
    {
        return "You received {$this->amount} from {$this->transfer->payer->name}";
    }
}
