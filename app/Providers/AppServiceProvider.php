<?php

namespace App\Providers;

use App\Clients\CustomNotificationApiClient;
use App\Clients\TransactionAuthorizerApiClient;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            TransactionAuthorizerApiClient::class,
            fn($app) => new TransactionAuthorizerApiClient(data_get($app, 'config.services.transaction_authorization.base_url'))
        );

        $this->app->bind(
            CustomNotificationApiClient::class,
            fn($app) => new CustomNotificationApiClient(data_get($app, 'config.services.notification_service.base_url'))
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
