<?php

namespace App\Rules;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Contracts\Validation\Rule;

class HasEnoughBalance implements Rule
{
    private User $user;
    private UserService $userService;

    /**
     * Create a new rule instance.
     *
     * @param  User  $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->userService = app(UserService::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     * @throws \App\Exceptions\InsufficientUserBalanceException
     */
    public function passes($attribute, $value): bool
    {
        return $this->userService
            ->setUser($this->user)
            ->checkHasEnoughBalance($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.custom.amount.enough_balance');
    }
}
