<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class TransferFromAccountScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if (auth()->guest()) {
            return $builder;
        }

        return $builder->where('user_payer_id', auth()->id());
    }
}
