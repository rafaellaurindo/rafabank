<?php

namespace App\Services;

use App\Exceptions\InvalidCredentialsException;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    /**
     * Attempt do login using an User's email and password.
     *
     * @param  string  $email
     * @param  string  $password
     * @return User
     *
     * @throws InvalidCredentialsException
     */
    public function login(string $email, string $password): User
    {
        if (!Auth::attempt(compact('email', 'password'))) {
            throw new InvalidCredentialsException();
        }

        return Auth::user();
    }

    /**
     * Create an User's auth token.
     *
     * @param  User  $user
     * @param  ?string  $tokenName
     * @return string
     */
    public function createToken(User $user, ?string $tokenName = 'api-token'): string
    {
        return $user->createToken($tokenName)->plainTextToken;
    }
}
