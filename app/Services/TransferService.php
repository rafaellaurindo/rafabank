<?php

namespace App\Services;

use App\Clients\TransactionAuthorizerApiClient;
use App\Exceptions\CompanyUserCannotSendTransfersException;
use App\Exceptions\TransactionNotAuthorizedException;
use App\Models\Transfer;
use App\Models\User;
use Exception;

class TransferService
{
    public Transfer $transfer;
    public UserService $userService;
    public TransactionAuthorizerApiClient $authorizerClient;

    public function __construct(UserService $userService, TransactionAuthorizerApiClient $authorizerClient)
    {
        $this->userService = $userService;
        $this->authorizerClient = $authorizerClient;
    }

    /**
     * Check if the transfer property is set.
     *
     * @return $this
     * @throws \Exception
     */
    public function checkIsTransferSet(): self
    {
        if (!isset($this->transfer)) {
            throw new Exception('Transfer is not set.', 500);
        }

        return $this;
    }

    /**
     * Get Transfer object.
     *
     * @return Transfer
     */
    public function getTransfer(): Transfer
    {
        return $this->transfer;
    }

    /**
     * Create a Transfer.
     *
     * @param  User  $payer
     * @param  User  $payee
     * @param  float  $amount
     * @return $this
     *
     */
    public function create(User $payer, User $payee, float $amount): self
    {
        $transfer = new Transfer;
        $transfer->user_payer_id = $payer->id;
        $transfer->user_payee_id = $payee->id;
        $transfer->amount = $amount;
        $transfer->save();

        $this->transfer = $transfer;

        return $this;
    }

    /**
     * Check if user can send transfers.
     *
     * @param  User  $user
     * @return \App\Services\TransferService
     * @throws CompanyUserCannotSendTransfersException
     */
    public function checkUserCanSendTransfers(User $user): self
    {
        if ($user->type === User::TYPE_COMPANY) {
            throw new CompanyUserCannotSendTransfersException();
        }

        return $this;
    }

    /**
     * Check if transaction can be authorized.
     *
     * @return $this
     * @throws \App\Exceptions\TransactionNotAuthorizedException
     * @throws \Exception
     */
    public function checkForAuthorization(): self
    {
        $this->checkIsTransferSet();

        if ($this->authorizerClient->checkAuthorizationToPerformTransfer($this->transfer)) {
            return $this;
        }

        throw new TransactionNotAuthorizedException();
    }
}
