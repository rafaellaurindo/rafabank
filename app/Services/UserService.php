<?php

namespace App\Services;

use App\Exceptions\InsufficientUserBalanceException;
use App\Exceptions\InvalidUserTypeException;
use App\Models\User;
use PHPUnit\Util\Exception;

class UserService
{
    private User $user;

    /**
     * Set User.
     *
     * @param  \App\Models\User  $user
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Check if the user property is set.
     *
     * @return $this
     */
    public function checkIsUserSet(): self
    {
        if (!isset($this->user)) {
            throw new Exception('User is not set.', 500);
        }

        return $this;
    }

    /**
     * Create an User model.
     *
     * @param  string  $name
     * @param  string  $email
     * @param  string  $document
     * @param  string  $password
     * @param  string  $type
     * @return \App\Models\User
     *
     * @throws \App\Exceptions\InvalidUserTypeException
     */
    public function create(string $name, string $email, string $document, string $password, string $type): User
    {
        if (!in_array($type, User::TYPES)) {
            throw new InvalidUserTypeException();
        }

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->document = preg_replace('/[^0-9]/', '', $document);
        $user->password = bcrypt($password);
        $user->type = $type;
        $user->save();

        return $user;
    }

    /**
     * Check if user has enough balance.
     *
     * @param  float  $amount
     * @param  bool  $throwsException
     * @return bool
     * @throws \App\Exceptions\InsufficientUserBalanceException
     */
    public function checkHasEnoughBalance(float $amount, bool $throwsException = false): bool
    {
        $this->checkIsUserSet();

        if ($this->user->balance >= $amount) {
            return true;
        }

        if ($throwsException) {
            throw new InsufficientUserBalanceException();
        }

        return false;
    }

    /**
     * Increases User current balance.
     *
     * @param  float  $amount
     * @return float new balance
     */
    public function increaseBalance(float $amount): float
    {
        $this->checkIsUserSet();

        $this->user->balance += $amount;
        $this->user->save();

        return $this->user->balance;
    }

    /**
     * Increases User current balance.
     *
     * @param  float  $amount
     * @return float new balance
     * @throws \App\Exceptions\InsufficientUserBalanceException
     */
    public function decreaseBalance(float $amount): float
    {
        $this->checkIsUserSet();
        $this->checkHasEnoughBalance($amount, true);

        $this->user->balance -= $amount;
        $this->user->save();

        return $this->user->balance;
    }
}
