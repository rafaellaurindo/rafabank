<?php

if (!function_exists('currency')) {
    /**
     * Format float to BRL curency string.
     *
     * @param  float  $amount
     * @return string
     */
    function currency(float $amount): string
    {
        return 'R$ ' . number_format($amount, 2, ',', '.');
    }
}
