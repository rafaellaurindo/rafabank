<?php

namespace Database\Factories;

use App\Models\Transfer;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransferFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transfer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_payer_id' => User::factory()->person(),
            'user_payee_id' => User::factory(),
            'amount' => $this->faker->randomFloat(2, 1, 10000),
        ];
    }

    public function fromUser(User $user)
    {
        return $this->state([
            'user_payer_id' => $user->id,
        ]);
    }

    public function toUser(User $user)
    {
        return $this->state([
            'user_payee_id' => $user->id,
        ]);
    }
}
