<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userType = $this->faker->randomElement(User::TYPES);

        return [
            'name' => match ($userType) {
                User::TYPE_PERSON => $this->faker->name(),
                User::TYPE_COMPANY => $this->faker->company(),
            },
            'document' => match ($userType) {
                User::TYPE_PERSON => $this->faker->cpf(false),
                User::TYPE_COMPANY => $this->faker->cnpj(false),
            },
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'type' => $userType,
            'balance' => 0,
        ];
    }

    /**
     * Indicate that the user should be a company.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function company()
    {
        return $this->state(fn() => [
            'document' => $this->faker->cnpj(false),
            'type' => User::TYPE_COMPANY,
        ]);
    }

    /**
     * Indicate that the user should be a person.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function person()
    {
        return $this->state(fn() => [
            'document' => $this->faker->cpf(false),
            'type' => User::TYPE_PERSON,
        ]);
    }
}
