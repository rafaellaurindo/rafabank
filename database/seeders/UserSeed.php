<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::whereDocument('11111111111')->exists()) {
            User::factory()->create([
                "name" => "John Doe",
                "document" => "11111111111",
                "email" => "john@doe.com",
                "password" => bcrypt("j0hnZ1nho"),
                "type" => User::TYPE_PERSON,
                'balance' => 100,
            ]);
        }

        if (!User::whereDocument('99999999999')->exists()) {
            User::factory()->create([
                "name" => "Walmart Center",
                "document" => "99999999999",
                "email" => "contact@walmartcenter.com",
                "password" => bcrypt("wal@center"),
                "type" => User::TYPE_COMPANY,
                'balance' => 100,
            ]);
        }
    }
}
