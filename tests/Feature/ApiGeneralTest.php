<?php

namespace Tests\Feature;

use Tests\TestCase;

class ApiGeneralTest extends TestCase
{
    public function testCanGetApiHealthy()
    {
        $this->get('/api')
            ->assertSuccessful()
            ->assertJson([
                'api_version' => config('app.version'),
                'status' => 'ok',
            ]);
    }

    public function testRedirectsCorrectlyToDocumentation()
    {
        $this->get('/')
            ->assertRedirect('https://documenter.getpostman.com/view/13892160/Tzm6mwHE');
    }
}
