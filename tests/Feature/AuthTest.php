<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testCanLoginUsingValidCredentials()
    {
        $password = 'str0ngp@ssw0rd';
        $balance = $this->faker->randomFloat(2, 10, 1000);

        /** @var User $user */
        $user = User::factory()->create([
            'password' => bcrypt($password),
            'balance' => $balance,
        ]);

        $this->post('/api/auth/login', [
            "email" => $user->email,
            "password" => $password,
        ])
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'token'])
            ->assertJson([
                'user' => [
                    'id' => $user->id,
                    'document' => $user->document,
                    'email' => $user->email,
                    'balance' => $balance,
                ],
            ]);

        $this->assertDatabaseHas('personal_access_tokens', [
            'tokenable_type' => User::class,
            'tokenable_id' => $user->id,
        ]);
    }

    public function testCantLoginUsingInvalidEmail()
    {
        $this->post('/api/auth/login', [
            "email" => 'invalid-email',
            "password" => 'some-password',
        ])
            ->assertStatus(422)
            ->assertJsonPath('data.email', [trans('validation.email', ['attribute' => 'email'])]);
    }

    public function testCantLoginUsingInvalidCredentials()
    {
        $this->post('/api/auth/login', [
            "email" => $this->faker->email,
            "password" => 'some-password',
        ])
            ->assertUnauthorized()
            ->assertJson([
                'error' => 'auth.invalid_credentials',
                'message' => 'Invalid credentials.',
            ]);
    }

    public function testCanGetCurrentUser()
    {
        $balance = $this->faker->randomFloat(2, 10, 1000);

        /** @var User $user */
        $user = User::factory()->create([
            'balance' => $balance,
        ]);

        $this
            ->actingAs($user, 'sanctum')
            ->get('/api/auth/user')
            ->assertSuccessful()
            ->assertJson([
                'id' => $user->id,
                'document' => $user->document,
                'email' => $user->email,
                'balance' => $balance,
            ]);
    }

    public function testCantGetCurrentUserWithoutAuthentication()
    {
        $this->get('/api/auth/user')->assertUnauthorized();
    }

    public function testCanRevokeToken()
    {
        $password = 'str0ngp@ssw0rd';

        /** @var User $user */
        $user = User::factory()->create([
            'password' => bcrypt($password),
        ]);

        Sanctum::actingAs($user, ['*']);

        $this
            ->put('/api/auth/revoke')
            ->assertNoContent();

        $this->assertDatabaseMissing('personal_access_tokens', [
            'tokenable_type' => User::class,
            'tokenable_id' => $user->id,
        ]);
    }

    public function testCantRevokeTokenWithoutAuthentication()
    {
        $this->put('/api/auth/revoke')->assertUnauthorized();
    }
}
