<?php

namespace Tests\Feature;

use App\Clients\TransactionAuthorizerApiClient;
use App\Models\Transfer;
use App\Models\User;
use App\Notifications\ReceivedTransferNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class TransferTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testUserCantTransferWithoutEnoughBalance()
    {
        $initialPayerBalance = $this->faker->randomFloat(2, 10, 100);

        $payer = User::factory()
            ->create([
                'balance' => $initialPayerBalance,
            ]);

        $payee = User::factory()
            ->person()
            ->create([
                'balance' => 0,
            ]);

        $this
            ->actingAs($payer, 'sanctum')
            ->post('/api/transfers', [
                'payee_id' => $payee->id,
                'amount' => $initialPayerBalance + 0.1,
            ])
            ->assertStatus(422)
            ->assertJsonPath('data.amount', [trans('validation.custom.amount.enough_balance')]);

        $this->assertDatabaseCount('transfers', 0);

        $this->assertDatabaseHas('users', [
            'id' => $payer->id,
            'balance' => $initialPayerBalance,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $payee->id,
            'balance' => 0,
        ]);
    }

    public function testCompanyCantSentTransfers()
    {
        $initialPayerBalance = $this->faker->randomFloat(2, 10, 100);

        $payer = User::factory()
            ->company()
            ->create([
                'balance' => $initialPayerBalance,
            ]);

        $payee = User::factory()->create([
            'balance' => 0,
        ]);

        $this
            ->actingAs($payer, 'sanctum')
            ->post('/api/transfers', [
                'payee_id' => $payee->id,
                'amount' => $initialPayerBalance,
            ])
            ->assertForbidden()
            ->assertJsonPath('error', 'transfer.company_cant_sent_transfers');

        $this->assertDatabaseCount('transfers', 0);

        $this->assertDatabaseHas('users', [
            'id' => $payer->id,
            'balance' => $initialPayerBalance,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $payee->id,
            'balance' => 0,
        ]);
    }

    public function testUserCantSentTransfersToInvalidPayee()
    {
        $initialPayerBalance = $this->faker->randomFloat(2, 10, 100);

        $payer = User::factory()
            ->person()
            ->create([
                'balance' => $initialPayerBalance,
            ]);

        $this
            ->actingAs($payer, 'sanctum')
            ->post('/api/transfers', [
                'payee_id' => 12333,
                'amount' => $initialPayerBalance,
            ])
            ->assertStatus(422)
            ->assertJsonPath('data.payee_id', [trans('validation.exists', ['attribute' => 'payee'])]);

        $this->assertDatabaseCount('transfers', 0);

        $this->assertDatabaseHas('users', [
            'id' => $payer->id,
            'balance' => $initialPayerBalance,
        ]);
    }

    public function testUserCanShowASentTransfer()
    {
        $user = User::factory()
            ->person()
            ->create();

        $transfer = Transfer::factory()
            ->fromUser($user)
            ->create();

        $this
            ->actingAs($user, 'sanctum')
            ->get("/api/transfers/{$transfer->id}")
            ->assertSuccessful()
            ->assertJson([
                'id' => $transfer->id,
                'amount' => $transfer->amount,
                'payer' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ],
                'payee' => [
                    'id' => $transfer->payee->id,
                    'name' => $transfer->payee->name,
                    'email' => $transfer->payee->email,
                ],
            ]);
    }

    public function testUserCanShowTransferFromAnotherUser()
    {
        $user = User::factory()
            ->person()
            ->create();

        $transfer = Transfer::factory()
            ->fromUser($user)
            ->create();

        $randomUser = User::factory()->create();

        $this
            ->actingAs($randomUser, 'sanctum')
            ->get("/api/transfers/{$transfer->id}")
            ->assertNotFound();
    }

    public function testCantShowATransferAsGuest()
    {
        $transfer = Transfer::factory()->create();

        $this
            ->get("/api/transfers/{$transfer->id}")
            ->assertUnauthorized();
    }

    public function testUserCanListTheSentTransfers()
    {
        $user = User::factory()
            ->person()
            ->create();

        Transfer::factory()
            ->fromUser($user)
            ->count(5)
            ->create();

        Transfer::factory()
            ->count(30)
            ->create();

        $response = $this
            ->actingAs($user, 'sanctum')
            ->get("/api/transfers");

        $response
            ->assertSuccessful()
            ->assertJsonPath('from', 1)
            ->assertJsonPath('to', 5);

        $this->assertCount(5, $response->json('data'));
    }

    public function testCantListTransfersAsGuest()
    {
        $this
            ->get("/api/transfers")
            ->assertUnauthorized();
    }

    public function testPersonUserCanSendTransferToAnotherPersonalUser()
    {
        Notification::fake();

        $payer = User::factory()
            ->person()
            ->create([
                'balance' => 1000,
            ]);

        $payee = User::factory()
            ->person()
            ->create([
                'balance' => 0,
            ]);

        $this->partialMock(TransactionAuthorizerApiClient::class)
            ->shouldReceive('checkAuthorizationToPerformTransfer')
            ->andReturnTrue();

        $this
            ->actingAs($payer, 'sanctum')
            ->post("/api/transfers", [
                'payee_id' => $payee->id,
                'amount' => 300,
            ])
            ->assertSuccessful()
            ->assertJson([
                'amount' => 300,
                'payer' => [
                    'id' => $payer->id,
                    'name' => $payer->name,
                    'email' => $payer->email,
                ],
                'payee' => [
                    'id' => $payee->id,
                    'name' => $payee->name,
                    'email' => $payee->email,
                ],
            ]);

        Notification::assertSentTo($payee, ReceivedTransferNotification::class);

        $this->assertDatabaseHas('transfers', [
            'user_payer_id' => $payer->id,
            'user_payee_id' => $payee->id,
            'amount' => 300,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $payer->id,
            'balance' => 700,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $payee->id,
            'balance' => 300,
        ]);
    }

    public function testUserBalanceMustNotChangeWhenTransferIsNotAuthorized()
    {
        Notification::fake();

        $payer = User::factory()
            ->person()
            ->create([
                'balance' => 1000,
            ]);

        $payee = User::factory()
            ->person()
            ->create([
                'balance' => 0,
            ]);

        $this->partialMock(TransactionAuthorizerApiClient::class)
            ->shouldReceive('checkAuthorizationToPerformTransfer')
            ->andReturnFalse();

        $this
            ->actingAs($payer, 'sanctum')
            ->post("/api/transfers", [
                'payee_id' => $payee->id,
                'amount' => 300,
            ])
            ->assertForbidden()
            ->assertJson([
                'error' => 'transaction.not_authorized',
                'message' => 'The transaction was not authorized. Sorry for the inconvenience, please contact us.',
            ]);

        Notification::assertNothingSent();

        $this->assertDatabaseMissing('transfers', [
            'user_payer_id' => $payer->id,
            'user_payee_id' => $payee->id,
            'amount' => 300,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $payer->id,
            'balance' => 1000,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $payee->id,
            'balance' => 0,
        ]);
    }

}
