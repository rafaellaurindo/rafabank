<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testCanRegisterAnUserTypePerson()
    {
        $name = $this->faker->name();
        $type = User::TYPE_PERSON;
        $document = $this->faker->cpf(false);
        $email = $this->faker->email();

        $exceptedUserData = [
            "name" => $name,
            "document" => $document,
            "email" => $email,
            "type" => $type,
        ];

        $this->post('/api/users', array_merge($exceptedUserData, [
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
        ]))
            ->assertSuccessful()
            ->assertJson($exceptedUserData);

        $this->assertDatabaseHas('users', array_merge($exceptedUserData, ['balance' => 0]));
    }

    public function testCanRegisterAnUserTypeCompany()
    {
        $name = $this->faker->company();
        $type = User::TYPE_COMPANY;
        $document = $this->faker->cnpj(false);
        $email = $this->faker->email();

        $exceptedUserData = [
            "name" => $name,
            "document" => $document,
            "email" => $email,
            "type" => $type,
        ];

        $this->post('/api/users', array_merge($exceptedUserData, [
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
        ]))
            ->assertSuccessful()
            ->assertJson($exceptedUserData);

        $this->assertDatabaseHas('users', array_merge($exceptedUserData, ['balance' => 0]));
    }

    public function testCantRegisterUserUsingAnInvalidType()
    {
        $name = $this->faker->name();
        $type = $this->faker->randomElement(User::TYPES);
        $document = match ($type) {
            User::TYPE_PERSON => $this->faker->cpf(false),
            User::TYPE_COMPANY => $this->faker->cnpj(false),
        };
        $email = $this->faker->email();

        $this->post('/api/users', [
            "name" => $name,
            "document" => $document,
            "email" => $email,
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
            "type" => 'some-invalid-type',
        ])
            ->assertStatus(422)
            ->assertJsonPath('data.type.0', trans('validation.custom.user_type.in'));

        $this->assertDatabaseCount('users', 0);
    }

    public function testCantRegisterUserWithInvalidEmail()
    {
        $name = $this->faker->name();
        $type = $this->faker->randomElement(User::TYPES);
        $document = match ($type) {
            User::TYPE_PERSON => $this->faker->cpf(false),
            User::TYPE_COMPANY => $this->faker->cnpj(false),
        };

        $this->post('/api/users', [
            "name" => $name,
            "document" => $document,
            "email" => 'some-invalid-email',
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
            "type" => $type,
        ])
            ->assertStatus(422)
            ->assertJsonPath('data.email.0', trans('validation.email', ['attribute' => 'email']));

        $this->assertDatabaseCount('users', 0);
    }

    public function testCantRegisterUserWithExistingEmail()
    {
        /** @var User $existingUser */
        $existingUser = User::factory()->create();

        $name = $this->faker->name();
        $type = $this->faker->randomElement(User::TYPES);
        $document = match ($type) {
            User::TYPE_PERSON => $this->faker->cpf(false),
            User::TYPE_COMPANY => $this->faker->cnpj(false),
        };

        $this->post('/api/users', [
            "name" => $name,
            "document" => $document,
            "email" => $existingUser->email,
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
            "type" => $type,
        ])
            ->assertStatus(422)
            ->assertJsonPath('data.email.0', trans('validation.unique', ['attribute' => 'email']));

        $this->assertDatabaseCount('users', 1);
    }

    public function testCantRegisterUserWithExistingDocument()
    {
        /** @var User $existingUser */
        $existingUser = User::factory()->company()->create();

        $name = $this->faker->name();
        $email = $this->faker->email();
        $type = $this->faker->randomElement(User::TYPES);

        $this->post('/api/users', [
            "name" => $name,
            "document" => $existingUser->document,
            "email" => $email,
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
            "type" => $type,
        ])
            ->assertStatus(422)
            ->assertJsonPath('data.document.0', trans('validation.unique', ['attribute' => 'document']));

        $this->assertDatabaseCount('users', 1);
    }

    public function testCantRegisterACompanyUserUsingACpfDocument()
    {
        $this->post('/api/users', [
            "name" => $this->faker->company(),
            "document" => $this->faker->cpf(false),
            "email" => $this->faker->email(),
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
            "type" => User::TYPE_COMPANY,
        ])
            ->assertStatus(422)
            ->assertJsonPath('data.document.0', trans('validation.custom.document.cnpj'));

        $this->assertDatabaseCount('users', 0);
    }

    public function testCantRegisterAPersonUserUsingACnpjDocument()
    {
        $this->post('/api/users', [
            "name" => $this->faker->name(),
            "document" => $this->faker->cnpj(false),
            "email" => $this->faker->email(),
            "password" => "L@urind0",
            "password_confirmation" => "L@urind0",
            "type" => User::TYPE_PERSON,
        ])
            ->assertStatus(422)
            ->assertJsonPath('data.document.0', trans('validation.custom.document.cpf'));

        $this->assertDatabaseCount('users', 0);
    }
}
