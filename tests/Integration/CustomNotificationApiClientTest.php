<?php

namespace Tests\Integration;

use App\Clients\CustomNotificationApiClient;
use App\Exceptions\SendNotificationException;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class CustomNotificationApiClientTest extends TestCase
{
    private CustomNotificationApiClient $customNotificationApiClient;

    protected function setUp(): void
    {
        parent::setUp();
        $this->customNotificationApiClient = app(CustomNotificationApiClient::class);
    }

    public function testBaseUrlIsSetCorrectly()
    {
        $this->assertEquals(
            config('services.notification_service.base_url'),
            $this->customNotificationApiClient->getBaseUrl()
        );
    }

    public function testCanNotifyUser()
    {
        /** @var \App\Models\User $user */
        $user = User::factory()->create();
        $message = "Welcome";

        $expectedRequestUrl = $this->customNotificationApiClient->getBaseUrl() . '/notify?';

        $expectedRequestUrl .= http_build_query([
            'user_id' => $user->id,
            'message' => $message,
        ]);

        Http::fake([
            $expectedRequestUrl => Http::response([
                'message' => CustomNotificationApiClient::MESSAGE_SENT_RESPONSE,
            ]),
        ]);

        $this->assertTrue(
            $this->customNotificationApiClient->sendNotification($user, $message)
        );
    }

    public function testHandleErrorsOnSendNotification()
    {
        /** @var \App\Models\User $user */
        $user = User::factory()->create();
        $message = "Welcome";

        $expectedRequestUrl = $this->customNotificationApiClient->getBaseUrl() . '/notify?';

        $expectedRequestUrl .= http_build_query([
            'user_id' => $user->id,
            'message' => $message,
        ]);

        Http::fake([
            $expectedRequestUrl => Http::response([
                'message' => 'Internal Error',
            ], 502),
        ]);

        $this->expectException(SendNotificationException::class);
        $this->expectExceptionCode(502);

        $this->customNotificationApiClient->sendNotification($user, $message);
    }

    public function testHandleSendNotificationFailed()
    {
        /** @var \App\Models\User $user */
        $user = User::factory()->create();
        $message = "Welcome";

        $expectedRequestUrl = $this->customNotificationApiClient->getBaseUrl() . '/notify?';

        $expectedRequestUrl .= http_build_query([
            'user_id' => $user->id,
            'message' => $message,
        ]);

        Http::fake([
            $expectedRequestUrl => Http::response([
                'message' => 'Another message',
            ]),
        ]);

        $this->expectException(SendNotificationException::class);
        $this->expectExceptionCode(502);

        $this->customNotificationApiClient->sendNotification($user, $message);
    }
}
