<?php

namespace Tests\Integration;

use App\Clients\TransactionAuthorizerApiClient;
use App\Exceptions\GetTransactionAuthorizationException;
use App\Models\Transfer;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class TransactionAuthorizerApiClientTest extends TestCase
{
    private TransactionAuthorizerApiClient $transactionAuthorizer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transactionAuthorizer = app(TransactionAuthorizerApiClient::class);
    }


    public function testBaseUrlIsSetCorrectly()
    {
        $this->assertEquals(
            config('services.transaction_authorization.base_url'),
            $this->transactionAuthorizer->getBaseUrl()
        );
    }

    public function testCheckAuthorizationForTransferAuthorized()
    {
        /** @var Transfer $transfer */
        $transfer = Transfer::factory()->create();

        $expectedRequestUrl = $this->transactionAuthorizer->getBaseUrl() . '/8fafdd68-a090-496f-8c9a-3442cf30dae6?';

        $expectedRequestUrl .= http_build_query([
            'payer_id' => $transfer->payer->id,
            'payee_id' => $transfer->payee->id,
            'amount' => $transfer->amount,
            'device' => request()->getClientIp(),
        ]);

        Http::fake([
            $expectedRequestUrl => Http::response([
                'message' => TransactionAuthorizerApiClient::AUTHORIZED_TRANSFER_MESSAGE,
            ]),
        ]);


        $this->assertTrue(
            $this->transactionAuthorizer->checkAuthorizationToPerformTransfer($transfer)
        );
    }

    public function testCheckAuthorizationForTransferNotAuthorized()
    {
        /** @var Transfer $transfer */
        $transfer = Transfer::factory()->create();

        $expectedRequestUrl = $this->transactionAuthorizer->getBaseUrl() . '/8fafdd68-a090-496f-8c9a-3442cf30dae6?';

        $expectedRequestUrl .= http_build_query([
            'payer_id' => $transfer->payer->id,
            'payee_id' => $transfer->payee->id,
            'amount' => $transfer->amount,
            'device' => request()->getClientIp(),
        ]);

        Http::fake([
            $expectedRequestUrl => Http::response([
                'message' => 'Another message',
            ]),
        ]);

        $this->assertFalse(
            $this->transactionAuthorizer->checkAuthorizationToPerformTransfer($transfer)
        );
    }

    public function testThrowsCustomExceptionOnErrorsWhenCheckAuthorization()
    {
        /** @var Transfer $transfer */
        $transfer = Transfer::factory()->create();

        $expectedRequestUrl = $this->transactionAuthorizer->getBaseUrl() . '/8fafdd68-a090-496f-8c9a-3442cf30dae6?';

        $expectedRequestUrl .= http_build_query([
            'payer_id' => $transfer->payer->id,
            'payee_id' => $transfer->payee->id,
            'amount' => $transfer->amount,
            'device' => request()->getClientIp(),
        ]);

        Http::fake([
            $expectedRequestUrl => Http::response([
                'message' => 'Internal Error',
            ], 500),
        ]);

        $this->expectException(GetTransactionAuthorizationException::class);
        $this->expectExceptionCode(502);

        $this->transactionAuthorizer->checkAuthorizationToPerformTransfer($transfer);
    }
}
