<?php

namespace Tests\Unit;

use Tests\TestCase;

class CurrencyHelperTest extends TestCase
{
    public function testFormatCorrectsAmountWithDecimal()
    {
        $this->assertEquals(
            'R$ 145,23',
            currency(145.23)
        );
    }

    public function testFormatCorrectsAmountWithoutDecimal()
    {
        $this->assertEquals(
            'R$ 10.043,00',
            currency(10043)
        );
    }
}
