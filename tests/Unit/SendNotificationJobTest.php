<?php

namespace Tests\Unit;

use App\Jobs\SendNotificationJob;
use App\Models\Transfer;
use App\Models\User;
use App\Notifications\ReceivedTransferNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SendNotificationJobTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function testJobSendCorrectNotification()
    {
        Notification::fake();

        $user = User::factory()->create();
        $transfer = Transfer::factory()
            ->toUser($user)
            ->create();

        dispatch_sync(new SendNotificationJob($user, new ReceivedTransferNotification($transfer)));

        Notification::assertSentTo($user, ReceivedTransferNotification::class);
    }
}
